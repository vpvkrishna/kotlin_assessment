
/*
Write a Kotlin program to calculate the Sum of Natural Numbers Using Recursion
a. The number whose sum is to be found is stored in a variable number.
b. Initially, the addNumbers() is called from the main() function with 20 passed as
an argumen
 */
fun main(args: Array<String>) {
    val sum = addNumbers(20)
    println("Sum: "+sum)
}

fun addNumbers(num: Int): Int{
    if(num == 1) return 1

    return num + addNumbers(num-1)
}