/*
Write a Kotlin program to print the result of the following operations. Declare variables and
initialize them with given values .
a. -5 + 8 * 6
b. (55+9) % 9
c. 20 + -3*5 / 8
d. 5 + 15 / 3 * 2 - 8 % 3
 */

fun main(args: Array<String>){
    printResults()
}

fun printResults(){
    val exp1 = -5 + 8 * 6
    val exp2 = (55+9) % 9
    val exp3 = 20 + -3*5 / 8
    val exp4 = 5 + 15 / 3 * 2 - 8 % 3

    println(exp1)
    println(exp2)
    println(exp3)
    println(exp4)
}