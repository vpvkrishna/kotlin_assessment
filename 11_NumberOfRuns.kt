
/*
Any array may be viewed as a number of "runs" of equal numbers.
For example, the following array has two runs:
 1, 1, 1, 2, 2
Three 1's in a row form the first run, and two 2's form the second.
This array has two runs of length one:
 3, 4
And this one has five runs:
 1, 0, 1, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0
Implement the runs() function so that it returns the number
of runs in the given array.
 */

fun main(args: Array<String>) {
    runs(intArrayOf(1,1,2,2,2))
    runs(intArrayOf(3, 4))
    runs(intArrayOf(1,0,1,1,1,2,0,0,0,0,0,0,0))
}

fun runs(nums: IntArray): Int{
    var prev : Int? = 0
    var runs = 0

    for(n in nums){
        if(prev != n){
            prev = n
            runs++
        }
    }
    println("Runs: $runs")
    return runs
}