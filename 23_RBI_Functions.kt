/*
Create an interface called RBI with functions
bool createAccount()
float getBalance()
bool withDraw()
float Deposit(var amount : Int)
Create four classes SBI, ICICI, BOB, HSBC which implements the interface RBI.

 */

fun main(args: Array<String>) {
    var sbiAccount: RBI = SBI()
    perfomOperations(sbiAccount)

    var iciciAccount: RBI = ICICI()
    perfomOperations(iciciAccount)

    var hsbcAccount: RBI = HSBC()
    perfomOperations(hsbcAccount)

    var bobAccount: RBI = BOB()
    perfomOperations(bobAccount)

}

fun perfomOperations(account: RBI){
    val accountStatus = account.createAccount()
    println("Is ${account.javaClass.simpleName} Account Created? $accountStatus")
    account.getBalance();
    account.deposit(2000)
    account.withDraw(500)
    println()
}

interface RBI {
    fun createAccount(): Boolean
    fun getBalance(): Float
    fun withDraw(amount: Int): Boolean
    fun deposit(amount : Int): Float
}

class SBI: RBI {
    private var isCreatedAccount = false
    private var balance: Float = 0f


    override fun createAccount(): Boolean {
        isCreatedAccount = true
        balance = 1000f // initial amount        
        return isCreatedAccount
    }

    override fun getBalance(): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }
        println("Balance: $balance")
        return balance
    }

    override fun withDraw(amount: Int): Boolean {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        if(balance < amount){
            //throw Exception("Insufficient Amount")
            return false
        }
        balance -= amount
        println("Balance after WithDraw($amount): $balance")
        return true
    }

    override fun deposit(amount: Int): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        balance += amount
        println("Balance after Deposit($amount): $balance")
        return balance
    }

}

class ICICI: RBI {
    private var isCreatedAccount = false
    private var balance: Float = 0f


    override fun createAccount(): Boolean {
        isCreatedAccount = true
        balance = 500f // initial amount
        return isCreatedAccount
    }

    override fun getBalance(): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }
        println("Balance: $balance")
        return balance
    }

    override fun withDraw(amount: Int): Boolean {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        if(balance < amount){
            //throw Exception("Insufficient Amount")
            return false
        }
        balance -= amount
        println("Balance after WithDraw($amount): $balance")
        return true
    }

    override fun deposit(amount: Int): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        balance += amount
        println("Balance after Deposit($amount): $balance")
        return balance
    }

}

class BOB: RBI {
    private var isCreatedAccount = false
    private var balance: Float = 0f


    override fun createAccount(): Boolean {
        isCreatedAccount = true
        balance = 2000f // initial amount
        return isCreatedAccount
    }

    override fun getBalance(): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }
        println("Balance: $balance")
        return balance
    }

    override fun withDraw(amount: Int): Boolean {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        if(balance < amount){
            //throw Exception("Insufficient Amount")
            return false
        }
        balance -= amount
        println("Balance after WithDraw($amount): $balance")
        return true
    }

    override fun deposit(amount: Int): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        balance += amount
        println("Balance after Deposit($amount): $balance")
        return balance
    }

}

class HSBC: RBI {
    private var isCreatedAccount = false
    private var balance: Float = 0f


    override fun createAccount(): Boolean {
        isCreatedAccount = true
        balance = 12000f // initial amount
        return isCreatedAccount
    }

    override fun getBalance(): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }
        println("Balance: $balance")
        return balance
    }

    override fun withDraw(amount: Int): Boolean {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        if(balance < amount){
            //throw Exception("Insufficient Amount")
            return false
        }
        balance -= amount
        println("Balance after WithDraw($amount): $balance")
        return true
    }

    override fun deposit(amount: Int): Float {
        if(!isCreatedAccount){
            throw Exception("Account not created")
        }

        balance += amount
        println("Balance after Deposit($amount): $balance")
        return balance
    }

}