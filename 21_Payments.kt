
/*
Define a class named Payment that contains a member variable of type double that
stores the amount of the payment and appropriate getter and setter methods. Also
create a method named paymentDetails that outputs an English sentence to describe
the amount of the payment.
Next, define a class named CashPayment that is derived from Payment. This class should
redefine the paymentDetails method to indicate that the payment is in cash. Include
appropriate constructor(s).
Define a class named CreditCardPayment that is derived from Payment. This class
should contain member variables for the name on the card, expiration date, and credit
card number. Include appropriate constructor(s). Finally, redefine the paymentDetails
method to include all credit card information in the printout.
Create a main method that creates at least two CashPayment and two
CreditCardPayment objects with different values and calls paymentDetails for each.
 */
fun main(args: Array<String>) {
    val cashPayment1 = CashPayment(30.5)
    cashPayment1.paymentDetails()

    val cashPayment2 = CashPayment(15.80)
    cashPayment2.paymentDetails()

    val creditCardPayment = CreditCardPayment(10.0, "Krishna V", "Nov 2021", "5789 9388 9847")
    creditCardPayment.paymentDetails()

    val creditCardPayment2 = CreditCardPayment(555.20, "Pavan V", "Dec 2021", "1234 1254 8758")
    creditCardPayment2.paymentDetails()
}

open class Payment(val amount: Double) {
    open fun paymentDetails(){
        println("Payment amount: $amount")
    }
}

class CashPayment(amount: Double) : Payment(amount){
    override fun paymentDetails(){
        println("Cash payment amount: $amount")
    }
}

class CreditCardPayment(amount: Double, val name: String, val expiryDate: String, val cardNumber: String) : Payment(amount){
    override fun paymentDetails() {
        println("Card payment amount: $amount & " +
                "Card holder name: $name & " +
                "Expiry Date: $expiryDate & " +
                "Card number: $cardNumber")
    }
}