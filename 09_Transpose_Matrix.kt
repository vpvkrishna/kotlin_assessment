import com.mobile.mytestapp.tranposeMatrix

/*
Write a Kotlin Program to display a transpose of a matrix
Matrix
a11 a12 a13
a21 a22 a23
Transposed Matrix
a11 a21
a12 a22
a13 a23
 */

fun main(args: Array<String>){
    val matrix : Array<Array<String?>> =
        arrayOf(arrayOf<String?>("a11","a12","a13"),
            arrayOf<String?>("a21","a22","a23"))
    displayMatrix(matrix)
    val transpose = transposeMatrix(matrix)
    displayMatrix(transpose)
}

fun transposeMatrix(matrix: Array<Array<String?>>): Array<Array<String?>>{
    val transpose: Array<Array<String?>> = Array(matrix[0].size) { arrayOfNulls<String>(matrix.size) }

    matrix.forEachIndexed { index, element ->
        element.forEachIndexed { index2, _ ->
            transpose[index2][index] = matrix[index][index2]
        }
    }
    return transpose
}

fun displayMatrix(matrix: Array<Array<String?>>){
    for(i in matrix){
        for(j in i){
            print(" $j")
        }
        println()
    }
    println()
}