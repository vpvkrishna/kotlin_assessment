
/*
Define a class to represent a bank account. Include the following:
Data members:
a. Name of the depositor b. Account number c. Type of account d. Balance amount in the
acccount
Member functions:
a. To assign initial values. b. To deposit an amount. c. To withdraw an amount after checking
the balance. d. To display name & balance.
Write a program to test your class. Modify the program for handling 10 customers
 */

fun main(args: Array<String>) {
    val listOfAccounts = mutableListOf<BankAccount>()
    listOfAccounts += BankAccount("Ravi", "1234 5674 4587", AccountType.SAVINGS, 0F)
    listOfAccounts += BankAccount("Kumar", "4444 5674 4587", AccountType.SAVINGS, 0F)
    listOfAccounts += BankAccount("Kiran", "8585 5674 1212", AccountType.CURRENT, 0F)
    listOfAccounts += BankAccount("Veeru", "1234 9634 4587", AccountType.SAVINGS, 0F)
    listOfAccounts += BankAccount("Krishna", "9614 5674 4587", AccountType.CURRENT, 0F)
    listOfAccounts += BankAccount("Pavan", "1234 5674 9647", AccountType.SAVINGS, 0F)
    listOfAccounts += BankAccount("Gopi", "8745 5674 6521", AccountType.SAVINGS, 0F)
    listOfAccounts += BankAccount("Mahi", "1234 6233 9898", AccountType.SAVINGS, 0F)
    listOfAccounts += BankAccount("Balu", "5454 5674 7865", AccountType.SAVINGS, 0F)
    listOfAccounts += BankAccount("Ramesh", "7895 5674 9898", AccountType.CURRENT, 0F)

    val selectedAccount = listOfAccounts.filter { it.accountNumber == "1234 5674 9647" }[0]
    selectedAccount.displayDetails()
    selectedAccount.depositAmount(5000)
    selectedAccount.displayDetails()
    selectedAccount.withdrawAmount(1500)
    selectedAccount.displayDetails()
}

class BankAccount(val name: String, val accountNumber: String, val accountType: AccountType, var balance: Float){

    init {
        initializeAccount()
    }

    fun initializeAccount(){
        balance = 1000F
    }

    fun depositAmount(amount: Int){
        balance += amount
    }

    fun withdrawAmount(amount: Int): Boolean{
        if(balance < amount){
            return false
        }
        balance -= amount
        return true
    }

    fun displayDetails(){
        println("Name: $name  Balance: $balance")
    }
}

enum class AccountType {
    SAVINGS, CURRENT
}