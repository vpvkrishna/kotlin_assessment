
/*
Write a Kotlin program to calculate difference between two time periods where Time is a
user defined class
a. class Time(internal var hours: Int, internal var minutes: Int, internal var
seconds: Int)
b. Write a function called “difference” which takes two arguments for start and stop
time and returns the difference between the two
 */

fun main(args: Array<String>){
    val time1 = Time(2, 10, 5)
    val time2 = Time(5, 20, 18)

    val diffTime = difference(time1, time2)
    println("Difference between times: $diffTime")
}

fun difference(startTime: Time, endTime: Time): Time{
    val diff = endTime.numberOfSeconds() - startTime.numberOfSeconds()
    //if(diff<0) diff=diff*-1
    val seconds = diff%60
    val minutes = (diff/60)%60
    val hours = diff/(60*60)
    //println("Difference in seconds: "+ diff)
    val diffTime = Time(hours, minutes, seconds)
    return diffTime
}

class Time(internal var hours: Int, internal var minutes: Int, internal var seconds: Int){
    fun numberOfSeconds() : Int{
        return (hours*60*60) + (minutes*60) + seconds
    }

    override fun toString(): String{
        return "(H:$hours M:$minutes S:$seconds)"
    }
}
