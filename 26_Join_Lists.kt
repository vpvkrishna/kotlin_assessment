
// Write a Kotlin Program to Join Two Lists

fun main(args: Array<String>) {
    val list = listOf("Apple", "Orange", "Banana", "Cherry")
    val list2 = listOf(1, 2, 3)
    val resultantList = list + list2

    println("List 1:$list")
    println("List 2:$list2")
    println("Resultant List:$resultantList")
}