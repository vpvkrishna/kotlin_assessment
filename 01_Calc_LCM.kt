
// Write a Kotlin program to calculate LCM using while Loop and if Statement

fun main(args: Array<String>) {

    calculateLCM(4, 10)
    calculateLCM(7, 15)
    calculateLCM(9, 15)
}

fun calculateLCM(num1: Int, num2: Int) : Int{
    var lcm : Int= if (num1>num2) num1 else num2

    while(true){
        if((lcm % num1 == 0) && (lcm % num2 == 0)){
            println("LCM($num1,$num2) : $lcm")
            break
        }
        lcm++
    }

    return lcm
}