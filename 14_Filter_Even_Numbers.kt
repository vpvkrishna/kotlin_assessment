
// Write a lambda function which will filter all the even numbers for the input List<Int>

fun main(args: Array<String>) {
    val list = listOf(1,2,3,4,5,6,7,8)
    println(list)
    val filteredList = filterEvens(list)
    println(filteredList)
}

fun filterEvens(numbers: List<Int>): List<Int> {
    return numbers.filter { it % 2 == 0 }
}