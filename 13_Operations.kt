
// Write a function for sum, difference, multiplication and division of two numbers which
// takes the operator as an additional argument. This function is to be implemented using when in Kotlin

fun main(args: Array<String>) {
    val add = operation(4, 5, "+")
    val diff = operation(14, 5, "-")
    val mul = operation(2, 9, "*")
    val div = operation(8, 2, "/")

    println("Add: $add")
    println("Difference: $diff")
    println("Multiplication: $mul")
    println("Division: $div")
}

fun operation(operand1: Int, operand2: Int, operation: String): Int {
    when(operation){
        "+" -> return operand1 + operand2
        "-" -> return operand1 - operand2
        "*" -> return operand1 * operand2
        "/" -> return operand1 / operand2
    }
    throw Exception("Invalid Operation")
}