
// Write an extension function to the existing string class which will replace all the spaces with “_” for the given string

fun main(args: Array<String>) {
    val text = "This is Kotlin program"
    val updatedText = text.replaceSpaceWithUnderscore();
    println("Text: $text")
    println("UpdatedText: $updatedText")
}

fun String.replaceSpaceWithUnderscore(): String {
    return replace(" ", "_")
}