/*
Write a program to create a class Book with the following data members: isbn, title and
price. Inherit the class Book to two derived classes: Magazine and Novel with the
following data members:
Magazine: type
Novel: author
Populate the details using constructors.
Create a magazine and Novel instance and display the details.
 */

fun main(args: Array<String>){
    val magazine = Magazine(1, "#HSNK001", "Legends", 100f)
    val novel = Novel("Krishna", "#UIROS3456", "Life of Gandhi", 250f)

    println("Magazine Details: ISBN-${magazine.isbn} Title-${magazine.title}" +
            " Price-${magazine.price} Type-${magazine.type}")

    println("Novel Details: ISBN-${novel.isbn} Title-${novel.title}" +
            " Price-${novel.price} Author-${novel.author}")
}

open class Book(val isbn: String, val title: String, val price: Float)
class Magazine(val type: Int, isbn: String, title: String, price: Float): Book(isbn, title, price)
class Novel(val author: String, isbn: String, title: String, price: Float): Book(isbn, title, price)
