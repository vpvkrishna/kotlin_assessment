
// Write a Kotlin program to Find Factorial of a number using for loop

fun main(args: Array<String>){
    val num = 3
    val fact = factorialOfNumber(num)
    println("Factorial of $num : $fact")
}

fun factorialOfNumber(num: Int): Int{
    if(num == 0) return 1

    var res = 1
    for(n in 1..num){
        res = res*n
    }
    return res
}