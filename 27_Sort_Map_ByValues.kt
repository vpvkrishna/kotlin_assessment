
// Write a Kotlin Program to Sort a Map by Values

fun main(args: Array<String>) {
    val map = mapOf(Pair(1, "Cat"), Pair(2, "Ant"), Pair(8, "Monkey"), Pair(5, "Dog"))
    println("Map:$map")

    val res = map.entries.sortedBy { it.value }.associateBy({ it.key }, { it.value })
    println("SortedMap: $res")
}