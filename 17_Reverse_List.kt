
// Write an extension function to the existing List class to reverse the list and return into a new list.

fun main(args: Array<String>){
    val list = listOf("Ant", "Apple", "Banana", "Orange", "PineApple")
    val reverseList = list.reverseList()
    println("List: $list")
    println("ReverseList: $reverseList")
}

fun List<String>.reverseList(): List<String> {
    val reverseList = mutableListOf<String>()
    for(index in 0 until size){
        reverseList += get(size - index - 1)
    }
    return reverseList
}