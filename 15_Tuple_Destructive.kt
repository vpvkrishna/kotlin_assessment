/*
Create a class to hold the members for runs, age, nom, name and a primary key as
player_number. Create a List of players with appropriate values for each of the
members and also ensure unique player_number in the List.
a) Write a function to return a tuple with all the members values for the given
player_number
b) Write a program to destructure the return value of the function and print the
individual values */

fun main(args: Array<String>) {
    val players = createListOfPlayers()
    val selectedPlayer = returnTuple(3, players)
    println("Selected player: $selectedPlayer")
    val result = destructiveValues(selectedPlayer.second!!)
    println("Player info $result")
}


data class Player(val player_number: Int, val runs: Int,
                  val age: Int, val nom: String, val name: String)

fun createListOfPlayers(): List<Player> {
    val list = listOf(
        Player(1, 20, 18, "A", "ABC"),
        Player(2, 22, 19, "B", "XYZ"),
        Player(3, 98, 39, "C", "CDE"),
        Player(4, 144, 24, "D", "IJK")
    )
    return list
}

fun destructiveValues(player: Player): String {
    val (no, runs, age, nom, name) = player
    return "No:$no, Runs:$runs, Age:$age, Nom:$nom, Name:$name"
}

fun returnTuple(no: Int, players: List<Player>): Pair<Int, Player?> {
    for(player in players) {
        if(player.player_number == no) {
            return Pair(no, player)
        }
    }
    return Pair(no, null)
}



