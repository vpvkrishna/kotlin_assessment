
// Write a Kotlin program to filter all the null values and odd numbers from a List
fun main(args: Array<String>) {
    val list = listOf(1, 8, 6, null, 77, null, 34, 11)
    println("List: $list")

    val filteredList = list.filter { it != null && it % 2 != 1 }
    println("Filter list: $filteredList")
}