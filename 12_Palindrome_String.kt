
// Palindrome: Implement a function which returns true if the given string is a palindrome or false if not

fun main(args: Array<String>){
    var isPalindrome = isPalindrome("Krishna")
    println("Palindrome: $isPalindrome")

    isPalindrome = isPalindrome("level")
    println("Palindrome: $isPalindrome")

    isPalindrome = isPalindrome("lool")
    println("Palindrome: $isPalindrome")
}

fun isPalindrome(str: String): Boolean{
    val len = str.length-1
    for(index in 0..len/2){
        if(str[index] != str[len-index]){
            return false
        }
    }
    return true
}