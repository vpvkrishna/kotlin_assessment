
// Implement indexOfMax() function so that it returns the index of the largest element in the array, or null if the array is empty

fun main(args: Array<String>){
    val maxIndex = indexOfMax(arrayOf(10, 15, 22, 8, 6, 3, 9))
    println("Index: $maxIndex")
    val maxIndex2 = indexOfMax(arrayOf())
    println("Index: $maxIndex2")
}

fun indexOfMax(nums: Array<Int>): Int?{
    var greater = 0
    var index: Int? = null
    for(i in 1..nums.size-1){
        if(nums[i] > greater){
            greater = nums[i]
            index = i
        }
    }
    return index
}