/*
Write a Kotlin Program to Sort Elements in Lexicographical Order (Dictionary Order)
For Example:
Input Array should consist:
val words = arrayOf("Ruby", "C", "Python", "Java")
Output should be :
C
Java
Python
Ruby
 */

fun main(args: Array<String>) {
    val words = arrayOf("Ruby", "C", "Python", "Java")

    val sortedWords = words.sortedArray()
    for(word in sortedWords) {
        println(word)
    }
}