
// Write a Kotlin program to find greatest number in an array

fun main(args: Array<String>){
    val num = greaterInArray(arrayOf(2, 8, 19, 37, 14, 23))
    println("Greatest number :"+ num)
}

fun greaterInArray(nums: Array<Int>): Int{
    var greater = 0
    for(n in nums){
        if(n > greater){
            greater = n
        }
    }
    return greater
}