
// Write a program to print month in words, based on input month in numbers.(using switch case)

fun main(args: Array<String>){
    val nameOfMonth = nameOfMonth(5)
    println("Name of month: $nameOfMonth")
}

fun nameOfMonth(month: Int): String {
    when(month){
        1 -> return "January"
        2 -> return "February"
        3 -> return "March"
        4 -> return "April"
        5 -> return "May"
        6 -> return "June"
        7 -> return "July"
        8 -> return "August"
        9 -> return "September"
        10 -> return "October"
        11 -> return "November"
        12 -> return "December"
    }
    return "Invalid Month"
}