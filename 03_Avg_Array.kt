
// Write a Kotlin program to calculate the average of all the elements in an Array using for loop

fun main(arr: Array<String>){
    val avg = averageOfArrayNumbers(arrayOf(1,2,4))
    println("Average of array numbers :"+avg)
}

fun averageOfArrayNumbers(elements: Array<Int>): Float{
    var sum = 0

    for(num in elements){
        sum+=num
    }
    return sum.toFloat()/elements.size;
}