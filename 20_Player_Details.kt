
/*
Create a class called player with the properties named runs and number_of_matches.
Overload the “+” operator to return the sum of total runs and number_of_matches for
two player objects
 */
fun main(args: Array<String>) {
    val player = PlayerDetails(12, 3)
    val player2 = PlayerDetails(72, 2)

    val result = player + player2
    println("$player + $player2 = $result")
}

data class PlayerDetails(val runs: Int, val noOfMatches: Int) {

    operator fun plus(player: PlayerDetails): PlayerDetails {
        return PlayerDetails(runs + player.runs, noOfMatches + player.noOfMatches)
    }
}