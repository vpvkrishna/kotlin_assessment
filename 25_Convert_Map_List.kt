
// Write a Kotlin Program to Convert Map to List

fun main(args: Array<String>){
    val map = getMap()
    println(map)

    val keyList = map.keys.toList()
    val valueList = map.values.toList()
    println("Keys: $keyList")
    println("Values: $valueList")
}

fun getMap(): Map<Int, String> {
    return mapOf(Pair(1, "ABC"), Pair(2, "XYX"), Pair(3, "LMN"), Pair(5, "UVW"))
}