/*

Take a nested list and return a single flattened list with all values except nil/null.
The challenge is to write a function that accepts an arbitrarily-deep nested list-like
structure and returns a flattened structure without any nil/null values.
For Example
input: [1,[2,3,null,4],[null],5]
output: [1,2,3,4,5]

 */

fun main(args: Array<String>) {
    val nested = getNestedList() // [1, [2, 3, null, 4], [null], 5]
    println(nested)
    val converted = convertToFlattened(nested) //[1, 2, 3, 4, 5]
    println(converted)
}

fun getNestedList(): List<*> {
    val list = listOf(2, 3, null, 4)
    val list2 = listOf(null)
    val nestedList = listOf(1, list, list2, 5)
    return nestedList
}

fun convertToFlattened(nested: List<*>): MutableList<Int> {
    val resultantList = mutableListOf<Int>()
    for(item in nested){
        if(item is List<*>){
            resultantList += convertToFlattened(item)
        }else if (item is Int){
            resultantList += item
        }
    }
    return resultantList
}
