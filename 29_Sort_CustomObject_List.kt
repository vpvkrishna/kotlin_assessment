
//Write a Kotlin Program to Sort a List of Custom Objects by  Property [ public
//class CustomObject(val customProperty: String) {}]
fun main(args: Array<String>) {
    val listObj = listOf(
        CustomObject("Maths"),
        CustomObject("Science"),
        CustomObject("English"),
        CustomObject("Hindi")
    )
    println(listObj)
    val sortedList = listObj.sortedBy { it.customProperty }
    println(sortedList)
}
class CustomObject(val customProperty: String) {
    override fun toString(): String {
        return customProperty
    }
}