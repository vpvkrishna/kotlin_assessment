/*
Design and develop an OO program in Kotlin to implement an application to
update/search Cricket Statistics. [use data class]
The abstract class named cricket is created. Two classes bat and bowl are derived from the
cricket.
Data Members :
 player_ name
 number_of_matches
 runs
 Member functions :
 update():- this function takes input statistics from the admin for Batsman and Bowler.
 write_to():- this function writes the objects' data to a List [ List<Bat>, List<Bowl>]
 Display():-this function is used to display the statistics for batsman and bowler.
 Search():-this function is used to search for a particular player and then display to
standard output.
 Fifties():- this function is used to return number of fifties made by a specific player.
 Hundreds():-this function is used to return number of hundreds made by a particular
player.
 Wickets():-this function is used to return number of wickets taken by a particular player.
 Overs():- this function is used to return number of overs bowled by a particular player.
 Use constructors to initialize the bat and bowler objects.
 Implement INHERITANCE by inheriting Cricket class into two new classes
- bat
- bowler
- Use appropriate data members and member functions inside derived classes.
 Create a List and store the records of the various players and display the stats on user's
request.
 Make your program more interactive.
 Test thoroughly for various inputs. 

 */

fun main(args: Array<String>) {
    val player = Bat("Ravi", 5, 190, 1, 1)
    val player2 = Bat("Raj", 90, 4500, 30, 20)
    val player3 = Bowl("Ram", 60, 350, 2, 0, 70, 340.5F)
    val player4 = Bowl("Balu", 75, 3050, 10, 3, 150, 424F)

    Cricket.writeTo(player)
    Cricket.writeTo(player2)
    Cricket.writeTo(player3)
    Cricket.writeTo(player4)
    println("PlayerList ${Cricket.getAllPlayers()}")

    var searchedPlayer = Cricket.search("Ravi")
    println("Searched Player before update: $searchedPlayer")

    val modifyPlayer = Bat("Ravi", 6, 270, 2, 1)
    Cricket.update(modifyPlayer)
    searchedPlayer = Cricket.search("Ravi")
    println("Searched Player after update: $searchedPlayer")

    println("PlayerList ${Cricket.getAllPlayers()}")

    val noSearchedPlayer = Cricket.search("ABC")
    println("no player exists : $noSearchedPlayer")

    val selectedPlayer = Cricket.search("Balu").singleOrNull()
    selectedPlayer?.display()
}

abstract class Cricket(val player_name: String, val number_of_matches: Int, val runs: Int,
                       protected val fifties: Int, protected val hundreds: Int){
    companion object {
        private val playerList = mutableListOf<Cricket>()

        fun getAllPlayers(): List<Cricket> {
            return playerList
        }
        fun update(cricket: Cricket) {
            for (i in 0 until playerList.size){
                if(playerList[i].player_name == cricket.player_name){
                    playerList[i] = cricket
                }
            }
        }
        fun writeTo(cricket: Cricket) {
            playerList += cricket
        }
        fun search(player_name: String): List<Cricket> {
            return playerList.filter { it.player_name == player_name }
        }
    }
    fun fifties(): Int {
        return fifties
    }
    fun hundreds(): Int {
        return hundreds
    }
    open fun wickets(): Int {
        return 0
    }
    open fun overs(): Float {
        return 0F
    }
    open fun display(){
        println("Cricketer ${player_name} stats : ${hundreds()} hundreds and ${fifties()} fifties in ${number_of_matches} matches")
    }
    override fun toString(): String {
        return "(PlayerName: $player_name, NoOfMatches: $number_of_matches, Runs: $runs, " +
                "fifties: $fifties, hundreds: $hundreds)"
    }
}

class Bat(player_name: String, number_of_matches: Int, runs: Int, fifties: Int, hundreds: Int)
    : Cricket(player_name, number_of_matches, runs, fifties, hundreds) {
}

class Bowl(player_name: String, number_of_matches: Int, runs: Int,
           fifties: Int, hundreds: Int, private val wickets: Int, private val overs: Float)
    : Cricket(player_name, number_of_matches, runs, fifties, hundreds) {
    override fun wickets(): Int {
        return wickets
    }
    override fun overs(): Float {
        return overs
    }
    override fun display(){
        println("Cricketer $player_name stats : ${hundreds()} hundreds, ${fifties()} fifties and $wickets wickets in $number_of_matches matches")
    }
    override fun toString(): String {
        return "(PlayerName: $player_name, NoOfMatches: $number_of_matches, Runs: $runs, " +
                "fifties: $fifties, hundreds: $hundreds, " +
                "Wickets: $wickets, Overs: $overs)"
    }
}